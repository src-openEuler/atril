# Conditional for release and snapshot builds. Uncomment for release-builds.
%global rel_build 1

# This is needed, because src-url contains branched part of versioning-scheme.
%global branch 1.22

# Settings used for build from snapshots.
%{!?rel_build:%global commit 5bba3723566489763aafaad3669c77f60a23d2e0}
%{!?rel_build:%global commit_date 20140122}
%{!?rel_build:%global shortcommit %(c=%{commit};echo ${c:0:7})}
%{!?rel_build:%global git_ver git%{commit_date}-%{shortcommit}}
%{!?rel_build:%global git_rel .git%{commit_date}.%{shortcommit}}
%{!?rel_build:%global git_tar %{name}-%{version}-%{git_ver}.tar.xz}

Name:          atril
Version:       %{branch}.3
Release:       6
Summary:       Document viewer
License:       GPLv2+ and LGPLv2+ and MIT
URL:           http://mate-desktop.org

# for downloading the tarball use 'spectool -g -R caja.spec'
# Source for release-builds.
%{?rel_build:Source0:     http://pub.mate-desktop.org/releases/%{branch}/%{name}-%{version}.tar.xz}
# Source for snapshot-builds.
%{!?rel_build:Source0:    http://git.mate-desktop.org/%{name}/snapshot/%{name}-%{commit}.tar.xz#/%{git_tar}}
Patch01:        CVE-2023-52076.patch
#fix from https://gitee.com/link?target=https%3A%2F%2Fgithub.com%2Fmate-desktop%2Fatril%2Fcommit%2Fce41df6467521ff9fd4f16514ae7d6ebb62eb1ed
Patch02:        CVE-2023-51698.patch
Patch03:        fix-build-error-check-aclocal-version.patch
Patch04:        backport-use-webkit2gtk-4.1.patch

BuildRequires:  gcc-c++
BuildRequires:  gtk3-devel
BuildRequires:  poppler-glib-devel
BuildRequires:  libXt-devel
BuildRequires:  libsecret-devel
BuildRequires:  libglade2-devel
BuildRequires:  libtiff-devel
BuildRequires:  libjpeg-turbo-devel
BuildRequires:  libspectre-devel
BuildRequires:  desktop-file-utils
BuildRequires:  gobject-introspection-devel
BuildRequires:  mate-common
BuildRequires:  cairo-gobject-devel
BuildRequires:  yelp-tools

# for the xps back-end
BuildRequires:  libgxps-devel
# for the caja properties page
#BuildRequires:  caja-devel
# for the dvi back-end
BuildRequires:  texlive-lib-devel
# for the djvu back-end
BuildRequires:  djvulibre-devel
# for epub back-end
BuildRequires:  pkgconfig(webkit2gtk-4.1)

Requires:       %{name}-libs%{?_isa} = %{version}-%{release}
#  fix (#974791)
Requires:       mate-desktop-libs
Requires:       mathjax

%description
Mate-document-viewer is simple document viewer.
It can display and print Portable Document Format (PDF),
PostScript (PS), Encapsulated PostScript (EPS), DVI, DJVU, epub and XPS files.
When supported by the document format, mate-document-viewer
allows searching for text, copying text to the clipboard,
hypertext navigation, table-of-contents bookmarks and editing of forms.


%package libs
Summary: Libraries for the mate-document-viewer

%description libs
This package contains shared libraries needed for mate-document-viewer.


%package devel
Summary: Support for developing back-ends for the mate-document-viewer
Requires: %{name}-libs%{?_isa} = %{version}-%{release}

%description devel
This package contains libraries and header files needed for
mate-document-viewer back-ends development.

%prep
%if 0%{?rel_build}
%autosetup -p1
%else
%autosetup -n %{name}-%{commit} -p1
%endif

%if 0%{?rel_build}
NOCONFIGURE=1 ./autogen.sh
%else # 0%{?rel_build}
# needed for git snapshots
NOCONFIGURE=1 ./autogen.sh
%endif # 0%{?rel_build}

%build
%configure \
        --disable-static \
        --disable-schemas-compile \
        --disable-caja \
        --enable-introspection \
        --enable-comics \
        --enable-dvi=yes \
        --enable-djvu=yes \
        --enable-t1lib=no \
        --enable-pixbuf \
        --enable-xps \
        --enable-epub

# remove unused-direct-shlib-dependency
sed -i -e 's! -shared ! -Wl,--as-needed\0!g' libtool

%make_build


%install
%{make_install}
%delete_la

%find_lang %{name} --with-gnome --all-name


%check
desktop-file-validate ${RPM_BUILD_ROOT}%{_datadir}/applications/atril.desktop


%files -f %{name}.lang
%doc README COPYING NEWS AUTHORS
%{_bindir}/*
%dir %{_datadir}/atril
%{_datadir}/atril/*
%{_datadir}/applications/atril.desktop
%{_datadir}/icons/hicolor/*/apps/atril.*
%{_libexecdir}/atrild
%{_datadir}/dbus-1/services/org.mate.atril.Daemon.service
%{_datadir}/glib-2.0/schemas/org.mate.Atril.gschema.xml
%{_datadir}/metainfo/atril.appdata.xml
%{_mandir}/man1/atril-*.1.*
%{_mandir}/man1/atril.1.*

%files libs
%{_libdir}/libatrilview.so.*
%{_libdir}/libatrildocument.so.*
%{_libdir}/atril/3/backends/
%{_libdir}/girepository-1.0/AtrilDocument-1.5.0.typelib
%{_libdir}/girepository-1.0/AtrilView-1.5.0.typelib

%exclude %{_datadir}/thumbnailers/atril.thumbnailer

%files devel
%dir %{_includedir}/atril/
%{_includedir}/atril/1.5.0/
%{_libdir}/libatrilview.so
%{_libdir}/libatrildocument.so
%{_libdir}/pkgconfig/atril-view-1.5.0.pc
%{_libdir}/pkgconfig/atril-document-1.5.0.pc
%{_datadir}/gir-1.0/AtrilDocument-1.5.0.gir
%{_datadir}/gir-1.0/AtrilView-1.5.0.gir
%{_datadir}/gtk-doc/html/libatrildocument-1.5.0/
%{_datadir}/gtk-doc/html/libatrilview-1.5.0/
%{_datadir}/gtk-doc/html/atril/


%changelog
* Thu Dec 26 2024 Funda Wang <fundawang@yeah.net> - 1.22.3-6
- use webkitgtk-4.1

* Thu Aug 15 2024 zhangxianting <zhangxianting@uniontech.com> - 1.22.3-5
- fix build error, check aclocal version

* Wed Apr 24 2024 peijiankang <peijiankang@kylinos.cn> - 1.22.3-4
- add CVE-2023-51698.patch

* Thu Feb 29 2024 peijiankang <peijiankang@kylinos.cn> - 1.22.3-3
- add CVE-2023-52076.patch

* Tue Jun 1 2021 douyan <douyan@kylinos.cn> - 1.22.3-2
- remove package atril-thumbnailer

* Tue Aug 25 2020 douyan <douyan@kylinos.cn> - 1.22.3-1
- disable caja support. Init package for openEuler

